package ATM

import io.github.aedans.cons.Cell
import io.github.aedans.cons.Cons
import io.github.aedans.cons.Nil
import io.github.aedans.cons.cons
import kategory.Option
import kategory.Option.None
import kategory.Option.Some

typealias BillCount = Pair<Int, Int>
typealias BillCountStack = Cons<BillCount>

fun getBills(available: List<BillCount>, amount: Int): Option<List<BillCount>> {
    if (amount == 0) {
        return Some(emptyList())
    }

    // sort into LIFO, smallest value first, highest last
    val availableSorted = available
        .sortedBy{ it.first }
        .fold(Nil as BillCountStack) { disbursals, billCount -> billCount cons disbursals }

    // recursive search, tries greedy first (all of highest bill), then successively 1 fewer of highest bill until none
    // of that bill, taking first found working solution.  it is entirely lazy (and therefore does not build up whole
    // search tree), by using asSequence and find.
    fun dfs(bills: BillCountStack, remaining: Int): Option<BillCountStack> =
        if (remaining == 0) Some(Nil) else when (bills) {
        Nil -> None
        is Cell -> {
            val (denomination, count) = bills.car
            Math.min(count, remaining / denomination).downTo(0).iterator().asSequence()
                .map{ used ->
                    dfs(bills.cdr, remaining - (denomination * used))
                        .map{ if (used > 0) BillCount(denomination, used) cons it else it }
                }
                .find{ it is Some<BillCountStack> } ?: None
        }
    }

    return dfs(availableSorted, amount)
}

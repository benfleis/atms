package ATM

import io.github.aedans.cons.consOf
import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.StringSpec
import kategory.Option.None
import kategory.Option.Some

class ATMTest : StringSpec() {
    init {
        val empty: List<BillCount> = consOf()
        val oneOne = consOf(BillCount(1, 1))
        val oneOnetwoTwos = consOf(BillCount(1, 1), BillCount(2, 2))
        val oneFive = consOf(BillCount(5, 1))
        val manyTwosAndFives = consOf(BillCount(2, 10), BillCount(5, 10))

        "1x1 satisfies get 1" {
            getBills(oneOne, 1) shouldBe Some(oneOne)
        }

        "empty billset and get 0 return empty" {
            getBills(empty, 0) shouldBe Some(empty)
        }

        "empty billset and get 5 return 5" {
            getBills(empty, 5) shouldBe None
        }

        "any billset and get 0 return empty" {
            getBills(oneOne, 0) shouldBe Some(empty)

            getBills(oneOnetwoTwos, 0) shouldBe Some(empty)
        }

        "2s + 5s satisfies get 6 → 2x3" {
            getBills(manyTwosAndFives, 6) shouldBe Some(consOf(BillCount(2, 3)))
        }

        "2s + 5s satisfies get 7 → 5+2" {
            getBills(manyTwosAndFives, 7) shouldBe Some(consOf(BillCount(5, 1), BillCount(2, 1)))
        }

        "2s + 5s satisfies get 11 → 5x1, 2x3" {
            getBills(manyTwosAndFives, 11) shouldBe Some(consOf(BillCount(5, 1), BillCount(2, 3)))
        }

        "5x1 fails for 10" {
            getBills(oneFive, 10) shouldBe None
        }
    }
}
